let http = require(`http`);    //1 require is needed to load node.js module.
//used the "require directive" to load Node.js
//HTTP is a protocol that allows the fetching or resources such as HTML docs.
http.createServer(function(request, response) {
	if (request.url =="/items" && request.method == "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database");
	}
}).listen(4000);
//2 A port is a virtual point where network connections start and end
//The server will be assigned to port 4000 via the ".listen(4000)"
console.log(`Server running at localhost: 4000`);
//3 When server is running, console will print the message